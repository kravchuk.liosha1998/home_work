const questions = new Randomizer({
  items: [
    //add you questions here
  ],
  storageName: "questions",
  title: "Questions randomizer",
  parent: document.querySelector(".container"),
});

const students = new Randomizer({
  items: [
    // add your group members here
  ],
  storageName: "students",
  title: "Students randomizer",
  parent: document.querySelector(".container"),
});
